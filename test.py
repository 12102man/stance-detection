import numpy as np
from scipy.sparse import hstack

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix

import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as data_utils

import pandas as pd

train_stances = pd.read_csv('./data/train_stances.csv')
train_bodies = pd.read_csv('./data/train_bodies.csv')

LABELS = {
    'unrelated': [1.0, 0.0, 0.0, 0.0],
    'agree': [0.0, 1.0, 0.0, 0.0],
    'discuss': [0.0, 0.0, 1.0, 0.0],
    'disagree': [0.0, 0.0, 0.0, 1.0]
}

DIGITS = {
    0: [1.0, 0.0, 0.0, 0.0],
    1: [0.0, 1.0, 0.0, 0.0],
    2: [0.0, 0.0, 1.0, 0.0],
    3: [0.0, 0.0, 0.0, 1.0]
}


def evaluate_model(model, test_loader):
    y_test = []
    y_pred = []
    correct = 0
    total = 0
    for features, labels in test_loader:
        output = model(features)
        for a, b in zip(output, labels):
            if DIGITS[torch.argmax(a, dim=0).tolist()] == b.tolist():
                correct += 1
            total += 1

            y_pred.append(torch.argmax(a, dim=0).tolist())
            y_test.append(b.tolist().index(1.0))
    print(f'Current accuracy: {(correct / total) * 100}%')
    print('Confusion_matrix:')
    print(confusion_matrix(y_test, y_pred, [0, 1, 2, 3]))



# Preparing data
data = []

# Creating a DataFrame with preprocessed data
for row in train_stances.iterrows():
    id = row[1]['Body ID']
    text = train_bodies.loc[train_bodies['Body ID'] == id]['articleBody'].values[0]
    data.append({'Title': row[1]['Headline'], 'Content': text, 'Stance': LABELS[row[1]['Stance']], 'StanceLabel': row[1]['Stance']})

df = pd.DataFrame(data)

for k in LABELS.keys():
    print(f"{k}: {df[df['StanceLabel'] == k].shape}")

# TF/IDF
count_title = CountVectorizer(stop_words='english', max_features=3000)
X_title = count_title.fit_transform(df.Title.values)
count_content = CountVectorizer(stop_words='english', max_features=3000)
X_content = count_content.fit_transform(df.Content.values)

corpus = list(np.hstack((df.Title.values, df.Content.values)).astype('str'))
tfidf = TfidfVectorizer(stop_words='english',
                        analyzer='word',
                        max_features=3000).fit_transform(corpus)
tfidf = tfidf.toarray()

# Cosine distances
X_cosine = []
for i in range(len(df)):
    X_cosine.extend(cosine_similarity(tfidf[i].reshape(1, -1),
                                      tfidf[i + len(df)].reshape(1, -1)))
X_cosine = np.asarray(X_cosine)
features = hstack((X_title, X_cosine, X_content.toarray()))
targets = [a for a in df.Stance.values]


# Train/test split
X_train, X_test, y_train, y_test = train_test_split(features,
                                                    targets,
                                                    test_size=0.2,
                                                    random_state=42, )


# Defining model
model = nn.Sequential(nn.Linear(6001, 100),
                      nn.ReLU(),
                      nn.Linear(100, 4),
                      nn.ReLU(),
                      nn.Softmax())

# Preparing data for learning
train_tensor = data_utils.TensorDataset(torch.tensor(X_train.toarray()).float(),
                                        torch.tensor(y_train))
test_tensor = data_utils.TensorDataset(torch.tensor(X_test.toarray()).float(),
                                       torch.tensor(y_test))

batch_size = 32

train_loader = data_utils.DataLoader(train_tensor,
                                     batch_size=batch_size,
                                     shuffle=True)
test_loader = data_utils.DataLoader(test_tensor,
                                    batch_size=batch_size,
                                    shuffle=True)

# Defining loss function
loss_function = nn.BCEWithLogitsLoss()

# Defining optimizer
optimizer = optim.Adam(model.parameters(), lr=0.003)

# Defining # of epochs
epochs = 300

# Learning
for e in range(epochs):
    loss_summ = 0
    for features, labels in train_loader:
        optimizer.zero_grad()
        out = model(features)
        loss = loss_function(out, labels)
        loss.backward()
        optimizer.step()

        loss_summ += loss.data.item()

    print('Epoch[{}/{}], loss: {:.6f}'.format(e + 1, epochs, loss_summ))
    evaluate_model(model, test_loader)
