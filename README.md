### Stance detection project

Mikhail Tkachenko, B17-DS-01

All the code is contained in file test.py
All pip dependencies required are located in requirements.txt. You can install them with 
```pip install -r requirements.txt```

Report is located in Report.pdf file.

If you have any questions, please contact me in Telegram: @man12102