import torch.nn as nn
import torch
import torch.optim as optim

model = nn.Sequential(nn.Linear(3, 3),
                      nn.Softmax())


x = torch.tensor([[1.0, 2.0, 3.0], [2.0, 3.0, 5.0], [1.0, 3.0, 5.0], [8.0, 6.0, 3.0]])
y = torch.tensor([[0, 1, 0], [0, 0, 1], [1, 0, 0], [0, 1, 0]])

criterion = nn.BCEWithLogitsLoss()
optimizer = optim.SGD(model.parameters(), lr=1e-1)

for epoch in range(20):
    output = model(x)
    loss = criterion(output, y.float())
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()
    print('Loss: {:.3f}'.format(loss.item()))
